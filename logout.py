#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.    If not, see <http://www.gnu.org/licenses/>.

import wx
import os

class MyFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(240,210), style=wx.BORDER_NONE)
        self.panel = wx.Panel(self)

        image = wx.Image('logout_logo.png', wx.BITMAP_TYPE_ANY)
        imageBitmap = wx.StaticBitmap(self.panel, wx.ID_ANY, wx.Bitmap(image))

        self.btnReboot = wx.Button(self.panel, -1, "Reboot", size=(120, 32))
        self.btnReboot.Bind(wx.EVT_BUTTON, self.rebootfunc)
        self.btnReboot.SetPosition((60,60))

        self.btnShutdown = wx.Button(self.panel, -1, "Shutdown", size=(120, 32))
        self.btnShutdown.Bind(wx.EVT_BUTTON, self.shutdownfunc)
        self.btnShutdown.SetPosition((60,95))

        self.btnLogout = wx.Button(self.panel, -1, "Logout", size=(120, 32))
        self.btnLogout.Bind(wx.EVT_BUTTON, self.exitfunc)
        self.btnLogout.SetPosition((60,130))

        self.btnCancel = wx.Button(self.panel, -1, "Cancel", size=(120, 32))
        self.btnCancel.Bind(wx.EVT_BUTTON, self.exitfunc)
        self.btnCancel.SetPosition((60,165))

        self.Centre()
        self.Show(True)

    def rebootfunc(self, event):
        os.system('sudo shutdown -r now')

    def shutdownfunc(self, event):
        os.system('sudo shutdown -h now')

    def exitfunc(self, event):
        exit()

app = wx.App(False)
frame = MyFrame(None, 'Piphone session')
app.MainLoop()
