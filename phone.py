#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.    If not, see <http://www.gnu.org/licenses/>.


import wx
import os
#import serial
#import RPi.GPIO as GPIO     
#import os, time

#GPIO.setmode(GPIO.BOARD)   

# Enable Serial Communication
#port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=1)

# Transmitting AT Commands to the Modem
# '\r\n' indicates the Enter key

#port.write('AT'+'\r\n')
#rcv = port.read(10)
#print rcv
#time.sleep(1)

#port.write('ATE0'+'\r\n')      # Disable the Echo
#rcv = port.read(10)
#print rcv
#time.sleep(1)

#port.write('AT+CMGF=1'+'\r\n')  # Select Message format as Text mode
#rcv = port.read(10)
#print rcv
#time.sleep(1)

#port.write('AT+CNMI=2,1,0,0,0'+'\r\n')   # New SMS Message Indications
#rcv = port.read(10)
#print rcv
#time.sleep(1)

# Sending a message to a particular Number

#port.write('AT+CMGS="XXXXXXXXXX"'+'\r\n')
#rcv = port.read(10)
#print rcv
#time.sleep(1)

#port.write('Hello User'+'\r\n')  # Message
#rcv = port.read(10)
#print rcv

#port.write("\x1A") # Enable to send SMS
#for i in range(10):
#    rcv = port.read(10)
#    print rcv

class WxTerm(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(480,320))
        self.panel = wx.Panel(self)

        style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL|wx.TE_RICH
        self.txtctrl = wx.TextCtrl(self.panel, wx.ID_ANY, pos=(20, 10), size=(210,48), style=style)
        self.txtctrl.AppendText("0")

        self.btn1 = wx.Button(self.panel, -1, "1", (50, 60), size=(48, 48))
        self.btn1.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '1'))
        self.btn2 = wx.Button(self.panel, -1, "2", (100, 60), size=(48, 48))
        self.btn2.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '2'))
        self.btn3 = wx.Button(self.panel, -1, "3", (150, 60), size=(48, 48))
        self.btn3.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '3'))

        self.btn4 = wx.Button(self.panel, -1, "4", (50, 110), size=(48, 48))
        self.btn4.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '4'))
        self.btn5 = wx.Button(self.panel, -1, "5", (100, 110), size=(48, 48))
        self.btn5.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '5'))
        self.btn6 = wx.Button(self.panel, -1, "6", (150, 110), size=(48, 48))
        self.btn6.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '6'))

        self.btn7 = wx.Button(self.panel, -1, "7", (50, 160), size=(48, 48))
        self.btn7.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '7'))
        self.btn8 = wx.Button(self.panel, -1, "8", (100, 160), size=(48, 48))
        self.btn8.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '8'))
        self.btn9 = wx.Button(self.panel, -1, "9", (150, 160), size=(48, 48))
        self.btn9.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '9'))

        self.btnSTAR = wx.Button(self.panel, -1, "*", (50, 210), size=(48, 48))
        self.btnSTAR.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '*'))
        self.btn0 = wx.Button(self.panel, -1, "0", (100, 210), size=(48, 48))
        self.btn0.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '0'))
        self.btnHASH = wx.Button(self.panel, -1, "#", (150, 210), size=(48, 48))
        self.btnHASH.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '#'))

        self.btnCALL = wx.Button(self.panel, -1, "CALL", (50, 260), size=(148, 48))
        self.btnCALL.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'CALL'))
# ******
        self.btnSETTINGS = wx.Button(self.panel, -1, "Setting", (300, 10), size=(70, 28))
        self.btnSETTINGS.Bind(wx.EVT_BUTTON, self.exitfunc)

        self.btnEXIT = wx.Button(self.panel, -1, "Exit", (390, 10), size=(70, 28))
        self.btnEXIT.Bind(wx.EVT_BUTTON, self.exitfunc)

        style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL|wx.TE_RICH
        self.txtctrl = wx.TextCtrl(self.panel, wx.ID_ANY, pos=(300, 50), size=(160,250), style=style)
        self.txtctrl.AppendText("Testname\n\nNoname\n\nMissing\n\nNothere")

        self.Centre()
        #self.Show(True)
        self.ShowFullScreen(True)

    def numCombo(self, event):
        global tcommand
        key = self.numcombo.GetValue()
        self.txtctrl.SetForegroundColour(wx.WHITE)
        self.txtctrl.AppendText(key)
        tcommand = tcommand + key

    def symCombo(self, event):
        global tcommand
        key = self.symcombo.GetValue()
        self.txtctrl.SetForegroundColour(wx.WHITE)
        self.txtctrl.AppendText(key)
        tcommand = tcommand + key

    def backfunc(self, event):
        self.txtctrl.Clear()
        self.txtctrl.SetForegroundColour(wx.RED)
        path_full = path + workdir + '$'
        self.txtctrl.AppendText(path_full)
        tcommand = ""
        tspace = 0

    def keyfunc(self, event, key):
        global tcommand
        self.txtctrl.SetForegroundColour(wx.WHITE)
        self.txtctrl.AppendText(key)
        tcommand = tcommand + key

    def spacefunc(self, event):
        global tcommand
        global tspace
        self.txtctrl.AppendText(' ')
        tcommand = tcommand + ' '
        if (tspace == 0):
            tspace = 1
        else:
            tspace = 2

    def enterfunc(self, event):
        global tcommand
        global tspace
        global workdir
        # is there a command?
        if (len(tcommand) <= 0):
            self.txtctrl.SetForegroundColour(wx.RED)
            path_full = '\n' + path + workdir + '$'
            self.txtctrl.AppendText(path_full)
        else:
            # test for 1, 2 and 3 spaces
            if (tspace == 1):
                x,y = tcommand.split(' ', 1 )
                if (x == 'cd'):
                    if (y == '..'):
                        # TODO get last / ?
                        x,y = tcommand.split('/', 1 )
                        workdir = x
                    elif (y == '~'):
                        workdir = ""
                    else:
                        workdir = workdir + '/' + y
                    result = ''
                    sys_cmd = x + ' ' + y 
                    os.system(sys_cmd)
                else:
                    result = subprocess.check_output([x, y])
            elif (tspace == 2):
                x,y,z = tcommand.split(' ', 2 )
                result = subprocess.check_output([x, y, z])
            elif (tspace == 3):
                x,y,z,q = tcommand.split(' ', 3 )
                result = subprocess.check_output([x, y, z, q])
            else:
                result = subprocess.check_output([tcommand])
            
            self.txtctrl.SetForegroundColour(wx.WHITE)
            self.txtctrl.AppendText('\n')
            self.txtctrl.AppendText(result)
            self.txtctrl.SetForegroundColour(wx.RED)
            path_full = path + workdir + '$'
            self.txtctrl.AppendText(path_full)
            tcommand = ""
            tspace = 0
#            self.txt.see(END)
        
    def exitfunc(self, event):
        exit()
        
app = wx.App(False)
frame = WxTerm(None, 'Piphone desktop')
app.MainLoop()

