#!/usr/bin/python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.    If not, see <http://www.gnu.org/licenses/>.

import sys
import wx
import subprocess
import os

path = 'pi@piphone:~'
workdir = "/piphone"
tcommand = ""
tspace = 0
x = ""
y = ""
z = ""

class WxTerm(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(480,320))
        self.panel = wx.Panel(self)

        style = wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL|wx.TE_RICH
        self.txtctrl = wx.TextCtrl(self.panel, wx.ID_ANY, size=(480,185), style=style)
        self.txtctrl.SetForegroundColour(wx.RED)
        self.txtctrl.SetBackgroundColour(wx.BLACK)
        path_full = path + workdir + '$'
        self.txtctrl.AppendText(path_full)

        self.btnQ = wx.Button(self.panel, -1, "Q", (5, 188), size=(38, 28))
        #self.btnQ.Bind(wx.EVT_BUTTON, self.keyfunc("q"))
        self.btnQ.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'q'))
        self.btnW = wx.Button(self.panel, -1, "W", (53, 188), size=(38, 28))
        self.btnW.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'w'))
        self.btnE = wx.Button(self.panel, -1, "E", (101, 188), size=(38, 28))
        self.btnE.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'e'))
        self.btnR = wx.Button(self.panel, -1, "R", (149, 188), size=(38, 28))
        self.btnR.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'r'))
        self.btnT = wx.Button(self.panel, -1, "T", (198, 188), size=(38, 28))
        self.btnT.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 't'))
        self.btnY = wx.Button(self.panel, -1, "Y", (245, 188), size=(38, 28))
        self.btnY.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'y'))
        self.btnU = wx.Button(self.panel, -1, "U", (293, 188), size=(38, 28))
        self.btnU.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'u'))
        self.btnI = wx.Button(self.panel, -1, "I", (341, 188), size=(38, 28))
        self.btnI.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'i'))
        self.btnO = wx.Button(self.panel, -1, "O", (389, 188), size=(38, 28))
        self.btnO.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'o'))
        self.btnP = wx.Button(self.panel, -1, "P", (437, 188), size=(38, 28))
        self.btnP.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'p'))

        self.btnA = wx.Button(self.panel, -1, "A", (29, 220), size=(38, 28))
        self.btnA.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'a'))
        self.btnS = wx.Button(self.panel, -1, "S", (77, 220), size=(38, 28))
        self.btnS.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 's'))
        self.btnD = wx.Button(self.panel, -1, "D", (125, 220), size=(38, 28))
        self.btnD.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'd'))
        self.btnF = wx.Button(self.panel, -1, "F", (173, 220), size=(38, 28))
        self.btnF.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'f'))
        self.btnG = wx.Button(self.panel, -1, "G", (221, 220), size=(38, 28))
        self.btnG.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'g'))
        self.btnH = wx.Button(self.panel, -1, "H", (269, 220), size=(38, 28))
        self.btnH.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'h'))
        self.btnJ = wx.Button(self.panel, -1, "J", (317, 220), size=(38, 28))
        self.btnJ.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'j'))
        self.btnK = wx.Button(self.panel, -1, "K", (365, 220), size=(38, 28))
        self.btnK.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'k'))
        self.btnL = wx.Button(self.panel, -1, "L", (413, 220), size=(38, 28))
        self.btnL.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'l'))

        self.btnSHIFT = wx.Button(self.panel, -1, "shift", (5, 252), size=(38, 28))
        self.btnSHIFT.Bind(wx.EVT_BUTTON, self.exitfunc)
        self.btnZ = wx.Button(self.panel, -1, "Z", (77, 252), size=(38, 28))
        self.btnZ.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'z'))
        self.btnX = wx.Button(self.panel, -1, "X", (125, 252), size=(38, 28))
        self.btnX.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'x'))
        self.btnC = wx.Button(self.panel, -1, "C", (173, 252), size=(38, 28))
        self.btnC.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'c'))
        self.btnV = wx.Button(self.panel, -1, "V", (221, 252), size=(38, 28))
        self.btnV.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'v'))
        self.btnB = wx.Button(self.panel, -1, "B", (269, 252), size=(38, 28))
        self.btnB.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'b'))
        self.btnN = wx.Button(self.panel, -1, "N", (317, 252), size=(38, 28))
        self.btnN.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'n'))
        self.btnM = wx.Button(self.panel, -1, "M", (365, 252), size=(38, 28))
        self.btnM.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, 'm'))
        self.btnBACK = wx.Button(self.panel, -1, "back", (437, 252), size=(38, 28))
        self.btnBACK.Bind(wx.EVT_BUTTON, self.backfunc)

        self.btnEXIT = wx.Button(self.panel, -1, "exit", (5, 284), size=(38, 28))
        self.btnEXIT.Bind(wx.EVT_BUTTON, self.exitfunc)
        self.btnSLASH = wx.Button(self.panel, -1, "/", (53, 284), size=(38, 28))
        self.btnSLASH.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '/'))
        self.btnDASH = wx.Button(self.panel, -1, "-", (101, 284), size=(38, 28))
        self.btnDASH.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '-'))

        num = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
        self.numcombo = wx.ComboBox(self.panel, choices=num, pos=(149, 284))
        self.numcombo.Bind(wx.EVT_COMBOBOX, self.numCombo)

        self.btnSPACE = wx.Button(self.panel, -1, "space", (211, 284), size=(60, 28))
        self.btnSPACE.Bind(wx.EVT_BUTTON, self.spacefunc)

        sym = ['~', '"', '@', '$', '%', '&', '(', ')', '=', '?', ':', '|', '<', '>', '*']
        self.symcombo = wx.ComboBox(self.panel, choices=sym, pos=(281, 284)) 
        self.symcombo.Bind(wx.EVT_COMBOBOX, self.symCombo)

        self.btnSTAR = wx.Button(self.panel, -1, "*", (341, 284), size=(38, 28))
        self.btnSTAR.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '*'))
        self.btnDOT = wx.Button(self.panel, -1, ".", (389, 284), size=(38, 28))
        self.btnDOT.Bind(wx.EVT_BUTTON, lambda event: self.keyfunc(event, '.'))
        self.btnENTER = wx.Button(self.panel, -1, "enter", (437, 284), size=(38, 28))
        self.btnENTER.Bind(wx.EVT_BUTTON, self.enterfunc)

        self.Centre()
        #self.Show(True)
        self.ShowFullScreen(True)

    def numCombo(self, event): 
        global tcommand
        key = self.numcombo.GetValue()
        self.txtctrl.SetForegroundColour(wx.WHITE)
        self.txtctrl.AppendText(key)
        tcommand = tcommand + key
        
    def symCombo(self, event): 
        global tcommand
        key = self.symcombo.GetValue()
        self.txtctrl.SetForegroundColour(wx.WHITE)
        self.txtctrl.AppendText(key)
        tcommand = tcommand + key

    def backfunc(self, event):
        self.txtctrl.Clear()
        self.txtctrl.SetForegroundColour(wx.RED)
        path_full = path + workdir + '$'
        self.txtctrl.AppendText(path_full)
        tcommand = ""
        tspace = 0

    def keyfunc(self, event, key):
        global tcommand
        self.txtctrl.SetForegroundColour(wx.WHITE)
        self.txtctrl.AppendText(key)
        tcommand = tcommand + key

    def spacefunc(self, event):
        global tcommand
        global tspace
        self.txtctrl.AppendText(' ')
        tcommand = tcommand + ' '
        if (tspace == 0):
            tspace = 1
        else:
            tspace = 2

    def enterfunc(self, event):
        global tcommand
        global tspace
        global workdir
        # is there a command?
        if (len(tcommand) <= 0):
            self.txtctrl.SetForegroundColour(wx.RED)
            path_full = '\n' + path + workdir + '$'
            self.txtctrl.AppendText(path_full)
        else:
            # test for 1, 2 and 3 spaces
            if (tspace == 1):
                x,y = tcommand.split(' ', 1 )
                if (x == 'cd'):
                    if (y == '..'):
                        # TODO get last / ?
                        x,y = tcommand.split('/', 1 )
                        workdir = x
                    elif (y == '~'):
                        workdir = ""
                    else:
                        workdir = workdir + '/' + y
                    result = ''
                    sys_cmd = x + ' ' + y
                    os.system(sys_cmd)
                else:
                    result = subprocess.check_output([x, y])
            elif (tspace == 2):
                x,y,z = tcommand.split(' ', 2 )
                result = subprocess.check_output([x, y, z])
            elif (tspace == 3):
                x,y,z,q = tcommand.split(' ', 3 )
                result = subprocess.check_output([x, y, z, q])
            else:
                result = subprocess.check_output([tcommand])
            
            self.txtctrl.SetForegroundColour(wx.WHITE)
            self.txtctrl.AppendText('\n')
            self.txtctrl.AppendText(result)
            self.txtctrl.SetForegroundColour(wx.RED)
            path_full = path + workdir + '$'
            self.txtctrl.AppendText(path_full)
            tcommand = ""
            tspace = 0
#            self.txt.see(END)
        
    def exitfunc(self, event):
        exit()
        
app = wx.App(False)
frame = WxTerm(None, 'Piphone desktop')
app.MainLoop()

