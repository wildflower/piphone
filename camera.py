#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.    If not, see <http://www.gnu.org/licenses/>.

from tkinter import *
from PIL import Image, ImageTk
from picamera import PiCamera
from datetime import datetime
import time

camera = PiCamera()
camera.resolution = (480, 320)
#camera.start_preview()
#time.sleep(2)
#camera.capture('bg.jpg')
#camera.stop_preview()

msg_text = "starting camera"

class Application(Frame):
    def update_img(self):
        camera.capture('bg.jpg')
        self.create_ui()
        root.after(1000, self.update_img)

    def take_pic(self):
        print("taking picture...")
        self.label_set()
        d = datetime.now()
        Ye = "%04d" % (d.year)
        Mo = "%02d" % (d.month)
        Da = "%02d" % (d.day)
        Ho = "%02d" % (d.hour)
        Mi = "%02d" % (d.minute)
        Ms = "%06d" % (d.microsecond)
        filename = "picam_" + str(Ye) +  str(Mo) +  str(Da) +  str(Ho) +  str(Mi) +  str(Ms) + ".jpg"
        print("taking picture %s" % (filename))
        camera.capture(filename)

    def label_set(self):
        print("label_set")
        global msg_text
        msg_text = "Taking picture..."
        self.create_ui()
        root.after(3000, self.label_reset)

    def label_reset(self):
        global msg_text
        msg_text = ""
        print("label_reset")

    def exit_cam(self):
        exit()

    def create_ui(self):
        image = Image.open("bg.jpg")
        photo = ImageTk.PhotoImage(image)
        label = Label(image=photo)
        label.image = photo # keep a reference!
        label.place(x=0, y=0)

        pic_button = Button(root, text ="Take picture", command=self.take_pic)
        pic_button.place(x=10, y=10)

        exit_button = Button(root, text ="Exit", command=self.exit_cam)
        exit_button.place(x=420, y=10)

        if (msg_text == ""):
            print("no messages")
        else:
            # draw label
            msg_label=Label(root, text=msg_text)
            msg_label.place(x=220, y=200)

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.create_ui()
        self.update_img()
        root.after(3000, self.label_reset)

root = Tk()
root.title("Program title")

#root.overrideredirect(1)
root.geometry('480x320+0+0')
root.attributes("-fullscreen", True)
#root.wm_attributes('-transparentcolor', root['bg'])
#root.wm_attributes("-topmost", 1)

#w, h = root.winfo_screenwidth(), root.winfo_screenheight()
#root.overrideredirect(1)
#root.geometry("%dx%d+0+0" % (w, h))

app = Application(master=root)
app.mainloop()
root.destroy()
