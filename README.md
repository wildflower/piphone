# Piphøne

A simple phone build around RPI model A+

https://www.youtube.com/watch?v=H1ZX8IZaE3E

Right now it's just a couple of python/shell-scripts on top of LXDE

partslist:

    Raspberry pi 3 Model A+
    3.5" Touchscreen Display (http://www.lcdwiki.com/3.5inch_RPi_Display)
    power for rpi
    camera
    gyro module
    SIM800L module
    realtime module
    buttons and pullup resistors
    misc. screws and hardware
