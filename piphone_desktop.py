#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.    If not, see <http://www.gnu.org/licenses/>.

import wx
import time
import os

time1 = time.strftime('%H:%M:%S')
date1 = time.strftime('%d/%m-%Y')

iconsB = ["./icons/power.png",
         "./icons/phone.png",
         "./icons/camera.png",
         "./icons/tool.png",
         "./icons/settings.png",
         "./icons/wifi.png"
]

iconsA = ["./icons/terminal.png",
         "./icons/headphones.png",
         "./icons/home.png",
         "./icons/message-square.png",
         "./icons/image.png",
         "./icons/globe.png"
]

class MyFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(480,320))
        self.panel = wx.Panel(self)

        # background
        image_file = 'bg_pink.jpg'
        bmp1 = wx.Image(image_file, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.bitmap1 = wx.StaticBitmap(self, -1, bmp1, (0, 0))

        # clock / date
        self.timeLabel = wx.StaticText(self, label=time1, pos=(194, 10))
        self.timeLabel.SetForegroundColour(wx.Colour(255,255,255))
        timefont = wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        self.timeLabel.SetFont(timefont)
        self.dateLabel = wx.StaticText(self, label=date1, pos=(204, 34))
        self.dateLabel.SetForegroundColour(wx.Colour(255,255,255))

        # icons
        ico_A0 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsA[0], wx.BITMAP_TYPE_ANY), (8, 172), (64,64))
        ico_A1 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsA[1], wx.BITMAP_TYPE_ANY), (88, 172), (64,64))
        ico_A2 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsA[2], wx.BITMAP_TYPE_ANY), (168, 172), (64,64))
        ico_A3 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsA[3], wx.BITMAP_TYPE_ANY), (248, 172), (64,64))
        ico_A4 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsA[4], wx.BITMAP_TYPE_ANY), (328, 172), (64,64))
        ico_A5 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsA[5], wx.BITMAP_TYPE_ANY), (408, 172), (64,64))
        ico_B0 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsB[0], wx.BITMAP_TYPE_ANY), (8, 244), (64,64))
        ico_B1 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsB[1], wx.BITMAP_TYPE_ANY), (88, 244), (64,64))
        ico_B2 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsB[2], wx.BITMAP_TYPE_ANY), (168, 244), (64,64))
        ico_B3 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsB[3], wx.BITMAP_TYPE_ANY), (248, 244), (64,64))
        ico_B4 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsB[4], wx.BITMAP_TYPE_ANY), (328, 244), (64,64))
        ico_B5 = wx.StaticBitmap(self, -1, wx.Bitmap(iconsB[5], wx.BITMAP_TYPE_ANY), (408, 244), (64,64))

        self.tick(None)
        self.timer = wx.Timer(self, -1)
        # update clock digits every second (1000ms)
        self.timer.Start(1000)
        self.Bind(wx.EVT_TIMER, self.tick)

        self.bitmap1.Bind(wx.EVT_LEFT_DOWN, self.getxy)

        #self.Show(True)
        self.ShowFullScreen(True)

    def tick(self, event):
        global time1
        global date1
        # get the current local time/date from the PC
        time2 = time.strftime('%H:%M:%S')
        date2 = time.strftime('%d/%m-%Y')
        # compare to saved time/date
        if time2 != time1:
            time1 = time2
            self.timeLabel.SetLabel(time1)
            self.panel.Layout()
        if date2 != date1:
            date1 = date2
            self.dateLabel.SetLabel(date1)
            self.panel.Layout()

    def getxy(self, event):
        #print("Position = ({0},{1})".format(event.x, event.y))
        # top row
        if (event.y > 172 and event.y < 236):
            # Terminal
            if (event.x > 8 and event.x < 72):
                os.system("./wxterm.py")
            # Music
            if (event.x > 88 and event.x < 152):
                os.system("qmmp")
                self.msg_box("Sorry... SMS not implemented yet.")
            # Filemanager
            if (event.x > 168 and event.x < 232):
                os.system("pcmanfm /home/pi")
            # SMS
            if (event.x > 248 and event.x < 312):
                self.msg_box("Sorry... SMS not implemented yet.")
            # Solitaire
            if (event.x > 328 and event.x < 392):
                os.system("sol")
            # Webbrowser
            if (event.x > 408 and event.x < 472):
                os.system("midori")
        # bottom row
        if (event.y > 244 and event.y < 312):
            # Logout
            if (event.x > 8 and event.x < 72):
                os.system("./logout.py")
            # Phone
            if (event.x > 88 and event.x < 152):
                os.system("./phone.py")
            # Camera
            if (event.x > 168 and event.x < 232):
                os.system("./camera.py")
            # Tools
            if (event.x > 248 and event.x < 312):
                self.msg_box("Sorry... Tools not implemented yet.")
            # lxappearance
            if (event.x > 328 and event.x < 392):
                os.system("lxappearance")
            # Wifi
            if (event.x > 408 and event.x < 472):
                self.msg_box("Sorry... Wifi control not implemented yet.")


    def msg_box(self, message, caption = 'pip system msg'):
        dlg = wx.MessageDialog(self, message, caption, wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

    def exitfunc(self):
        exit()

app = wx.App(False)
frame = MyFrame(None, 'Piphone desktop')
app.MainLoop()
